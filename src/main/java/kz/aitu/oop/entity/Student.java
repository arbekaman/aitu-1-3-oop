package kz.aitu.oop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String name;
    private int age;
    private double point;
    private String group;


    public String toString() {
        return "Student{" +"" +
                "group='" + group + '\''+
                ", name='" + name + '\'' +
                ", age=" + age +
                ", point=" + point +
                '}';
    }
}
