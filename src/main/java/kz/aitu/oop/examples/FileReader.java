package kz.aitu.oop.examples;

// Java Program to illustrate reading from Text File
// using Scanner Class
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) throws Exception {
      FileReader filereader = new FileReader();
        System.out.println(filereader.getFile());

    }

    public String getFile() throws FileNotFoundException {

        File file = new File("/Users/arbek/Desktop/file1.txt");
        Scanner sc = new Scanner(file);

        String result = "";
        while (sc.hasNextLine())
            result += sc.nextLine() + "\n";

        return result;
    }
}