package kz.aitu.oop.examples.Practice5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Main {

    public static boolean isInt(String row){
        try {
            int b = Integer.parseInt(row);
        }catch (NumberFormatException | NullPointerException e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean isDouble(String row2){
        try {
            Double b = Double.parseDouble(row2);
        }catch (NumberFormatException | NullPointerException e){
            System.out.println(e);
            return false;
        }
        return true;
    }


    public static void main(String[] args) {

        Error error = new Error();
        try {
            error.foo();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            System.err.println("Ещтене джок xD (1task) ");
        }

        File f = new File("/Users/arbek/Desktop/Student.txt")  ;

        try {
            if (!f.exists()) throw new FileNotFoundException();
            else {
                System.out.println("We found file");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        String num1 = "123";
        if( isInt(num1) == false) {
            System.err.println("in string dont have integer");
        }
        else {
            int c = Integer.parseInt(num1);
            System.out.println("Integer is " + c);
        }

        String num2 = "77.23";
        if( isDouble(num2) == false ) {
            System.err.println("in string dont have Double");
        }
        else {
            Double l = Double.parseDouble(num2);
            System.out.println("Double is " + l);
        }


    }


}
