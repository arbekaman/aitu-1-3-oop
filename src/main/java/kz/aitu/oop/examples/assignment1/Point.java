package kz.aitu.oop.examples.assignment1;


public class Point {

    public static void main(String[] args) {
        Point p1 = new Point (7,8);
        Point p2 = new Point(11,11);

        System.out.println(p2.calculateDistance(p1));
    }

    private double x;
    private double y;

    public Point(){
    }

    // return distance
    public double calculateDistance (Point point) {
        double distance = Math.sqrt(
                Math.pow ( point.getX() - this.getX(), 2) + Math.pow ( point.getY() - this.getY(), 2)
        );
    return distance;
    }

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}

