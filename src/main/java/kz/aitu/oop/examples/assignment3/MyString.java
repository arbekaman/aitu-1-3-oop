package kz.aitu.oop.examples.assignment3;

public class MyString {
    private int[] array;

    public MyString(int[] values ){
        array = values;
    }
    public int length() {
        return array.length;
    }
    public int valueAt(int a){
        if (a > array.length || a < (-1) ){
            return -1;
        }
        else{
            return array[a];
        }
    }
    public int count(int b){
        int count = 0;
        for (int i=0; i<array.length; i++){
            if (array[i] == b ){
                count++;
            }
        }
        return count;
    }

}
