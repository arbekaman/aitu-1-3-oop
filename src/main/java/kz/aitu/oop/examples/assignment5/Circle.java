package kz.aitu.oop.examples.assignment5;

public class Circle extends Shape {

    private Double radius;

    public Circle(){
        this.radius = 1.0;
    }
    public Circle( double rad ){
        this.radius=rad;
    }

    public Circle( double rad, String col, boolean bol ){
        super( col, bol );
        this.radius=rad;

    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Double getArea(){
        return 3.16*radius*radius;
    }

    public Double getPerimetr(){
        return 2*3.16*radius;
    }
    @Override
    public String toString(){
        return "A Circle with radius=" + radius + " , which is a subclass of " + super.toString();
    }
}
