package kz.aitu.oop.examples.assignment5;

    public class Rectangle extends Shape {

        private Double width;
        private Double length;

        public Rectangle(){
            this.width = 1.0;
            this.length = 1.0;
        }

        public Rectangle( double w, double l ){
            this.width=w;
            this.length=l;
        }

        public Rectangle(double w, double l, String clr, boolean bol ){
            super(clr, bol);
            this.width=w;
            this.length=l;

        }

        public Double getWidth() {
            return width;
        }

        public void setWidth(Double width) {
            this.width = width;
        }

        public Double getLength() {
            return length;
        }

        public void setLength(Double length) {
            this.length = length;
        }

        public Double getArea(){
            return width*length;
        }

        public Double getPerimetr(){
            return (width*length)*2;
        }
        @Override
        public String toString(){
            return "A Rectangle with width=" + width + " and length=" + length + ", which is a subclass of " + super.toString();
        }
    }
