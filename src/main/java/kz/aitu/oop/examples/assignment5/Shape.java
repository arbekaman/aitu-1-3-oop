package kz.aitu.oop.examples.assignment5;

public class Shape {

    private String color;
    private boolean filled;

    public Shape(){
        this.color = "green";
        this.filled = true;
    }

    public Shape(String clr, boolean bol) {
        color = clr;
        filled = bol;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean bool) {
        filled = bool;
    }

    @Override
    public String toString(){
        if(filled){
            return "A Shape with color of " + color + " and filled";
        }
        else {
            return "A Shape with color of " + color + " and nooooooooot filled";
        }
    }


}

