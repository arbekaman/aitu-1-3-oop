package kz.aitu.oop.examples.assignment5;

public class Sqare extends Rectangle {

    public Sqare(double side){
        super(side,side);
    }

    public Sqare( double side, String col, boolean bol ){
        super(  side, side, col, bol);
    }

    public double getSide(){
        return super.getWidth();
    }

    public void setSide( double side ){
        super.setLength( side );
        super.setWidth( side );
    }

    @Override
    public String toString(){
        return "A Square with side=" + super.getLength() + " , which is a subclass of" + super.toString() ;
    }




}
