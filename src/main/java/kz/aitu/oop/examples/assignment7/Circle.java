package kz.aitu.oop.examples.assignment7;

public  class Circle extends Shape {

    protected double radius;

    public Circle(){
        this.radius = 1.0;
    }
    public Circle( double rad ){
        this.radius=rad;
    }

    public Circle( double rad, String col, boolean bol ){
        super( col, bol );
        this.radius=rad;

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return 3.16*radius*radius;
    }

    @Override
    public double getPerimeter(){
        return 2*3.16*radius;
    }

    @Override
    public String toString(){
        return "Circle["+super.toString()+",radius="+radius+"]";
    }

}
