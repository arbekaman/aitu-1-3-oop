package kz.aitu.oop.examples.assignment7;

public class Rectangle extends Shape {

    protected Double width;
    protected Double length;

    public Rectangle(){
        this.width = 1.0;
        this.length = 1.0;
    }

    public Rectangle( double w, double l ){
        this.width=w;
        this.length=l;
    }

    public Rectangle(double w, double l, String clr, boolean bol ){
        super(clr, bol);
        this.width=w;
        this.length=l;

    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return width*length;
    }

    @Override
    public double getPerimeter(){
        return (width+length)*2;
    }

    @Override
    public String toString(){
        return ""+super.toString()+" ,width= "+width+" ,length= "+length+" ]";
    }

}
