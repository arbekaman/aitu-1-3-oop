package kz.aitu.oop.examples.assignment7;

public abstract class Shape {

    protected String color;
    protected boolean filled;

    public Shape(){
        this.color = "green";
        this.filled = true;
    }

    public Shape(String clr, boolean bol) {
        color = clr;
        filled = bol;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean bool) {
        filled = bool;
    }

    public abstract double getArea();
    public abstract double getPerimeter();


    @Override
    public String toString(){
        return "Shape[color="+color+",filled="+filled+"]";
    }



}

