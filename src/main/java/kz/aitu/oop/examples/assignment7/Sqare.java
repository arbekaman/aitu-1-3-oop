package kz.aitu.oop.examples.assignment7;

public class Sqare extends Rectangle {
    public Sqare(){
        this.width=width;
        this.length=length;
    }

    public Sqare(double side){
        width = side;
        length = side;
    }

    public Sqare( double side, String col, boolean bol ){
        super(  side, side, col, bol);
    }

    public double getSide(){
        return width;
    }

    public void setSide(double side){
        width = side;
        length = side;
    }


    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }


    @Override
    public String toString(){
        return "Sqare[Rectangle["+super.toString()+" ,width= "+width+" ,length= "+length+" ]";
    }





}
