package kz.aitu.oop.examples.assignment7.task2;

public interface Movable {

    public void moveRight();
    public void moveLeft();
    public void moveUp();
    public void moveDown();



}
