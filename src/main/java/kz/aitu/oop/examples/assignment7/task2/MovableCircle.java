package kz.aitu.oop.examples.assignment7.task2;

public class MovableCircle implements Movable {
    private int radius;
    private MovablePoint center;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public MovablePoint getCenter() {
        return center;
    }

    public void setCenter(MovablePoint center) {
        this.center = center;
    }

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int r){
        center.setXSpeed(xSpeed);
        center.setYSpeed(ySpeed);
        center.setY(y);
        center.setX(x);
        this.radius=r;
    }
    @Override
    public void moveUp(){
    center.setY(center.getY()+1);
    }
    @Override
    public void moveDown(){
        center.setY(center.getY()-1);
    }
    @Override
    public void moveRight(){
        center.setX(center.getX()+1);
    }
    @Override
    public void moveLeft(){
        center.setX(center.getX()-1);
    }
    @Override
    public String toString(){
        return "x = " + center.getX() +
                ", y = " + center.getY() +
                ", Xspeed = " + center.getXSpeed() +
                ", Yspeed = " + center.getYSpeed() +
                ", radius = " + this.radius + ".";
    }
}
