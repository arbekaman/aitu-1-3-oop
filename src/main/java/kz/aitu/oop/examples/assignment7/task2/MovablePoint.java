package kz.aitu.oop.examples.assignment7.task2;

import javax.servlet.http.PushBuilder;
import java.security.PublicKey;

public class MovablePoint implements Movable {
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getXSpeed() {
        return xSpeed;
    }

    public void setXSpeed(int xSpeed) {
        this.xSpeed = xSpeed;
    }

    public int getYSpeed() {
        return ySpeed;
    }

    public void setYSpeed(int ySpeed) {
        this.ySpeed = ySpeed;
    }
    public MovablePoint(int x, int y, int xSpeed, int ySpeed){
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void moveUp(){
        y++;
    }

    @Override
    public void moveDown(){
        y--;
    }

    @Override
    public void moveRight(){
        x++;
    }

    @Override
    public void moveLeft(){
        x--;
    }
    @Override
    public String toString(){
        return "x = " + this.x +
                ", y = " + this.y +
                ", xSpeed = " + this.xSpeed +
                ", ySpeed = " + this.ySpeed;
    }

}
