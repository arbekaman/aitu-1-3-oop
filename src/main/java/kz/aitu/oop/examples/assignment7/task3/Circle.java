package kz.aitu.oop.examples.assignment7.task3;

public class Circle implements GeometricObject {

    private double radius;

    public Circle(){
        this.radius = 1.0;
    }
    public Circle(double r){
        this.radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    @Override
    public double getPerimeter() {
        return ( this.radius * 2) * 3.14;
    }

    @Override
    public double getArea() {
        return ( this.radius * this.radius )*3.14;
    }
    @Override
    public String toString(){
        return "Radius is " + this.radius + ".";
    }


}
