package kz.aitu.oop.examples.assignment7.task3;

public interface GeometricObject {

    public double getPerimeter();
    public double getArea();

}
