package kz.aitu.oop.examples.assignment7.task3;

public interface Resizable {

    public void resize( int percent );

}
