package kz.aitu.oop.examples.assignment7.task3;

public class ResizableCircle extends Circle implements Resizable {


    public ResizableCircle( double r ){
        super(r);
    }
    @Override
    public void resize(int percent){
        super.setRadius ( (super.getRadius() * percent)/100 );
    }
    @Override
    public String toString(){
        return super.toString();
    }


}
