package kz.aitu.oop.examples.inheritance;

public class Bird {
    private String name;
    private Integer age;
    private Integer speed;

    public Bird(String n, Integer a) {
        this.name = n;
        this.age = a;
    }
    public String toString(){
        return name+" "+age+" "+speed;
    }
    public Integer getSpeed(){
        return speed;
    }

    public String getName() {
        return name;
    }

    public double getAge() {
        return age;
    }

    public void setName(String x) {
        this.name = x;
    }

    public void setAge(Integer y) {
        this.age = y;
    }

    public void setSpeed(Integer z){
        this.speed = z;
    }


}

