package kz.aitu.oop.examples.inheritance;

import java.security.PublicKey;

public class Eagle extends Bird {
    private Integer strength;

    public Eagle(String n, Integer a, Integer strength) {
        super(n, a);
        this.strength = strength;
    }
    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }
    @Override
    public String toString(){
        return super.toString()+" "+strength;
    }
}
