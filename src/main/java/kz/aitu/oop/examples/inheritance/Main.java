package kz.aitu.oop.examples.inheritance;

import ch.qos.logback.core.net.SyslogOutputStream;

public class Main {

    public static void main(String[] args) {

        Eagle eagle = new Eagle("Desert",3, 7);
        eagle.setSpeed(99);
        System.out.println(eagle.toString());

        System.out.println(eagle);

    }

}

