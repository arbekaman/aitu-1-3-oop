package kz.aitu.oop.examples.patterns;

import lombok.Data;

@Data
public class MySystem {
    private String name;
    private static int count = 0 ;

    private static MySystem mySystem = null;

    public MySystem(){
        count++;
    }

    public static MySystem getInstance(){
        if (mySystem == null){
            mySystem = new MySystem();
        }
        return mySystem;
    }
    @Override
    public String toString(){
        return name + ' ' + count;
    }

}
