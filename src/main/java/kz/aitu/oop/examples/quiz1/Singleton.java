package kz.aitu.oop.examples.quiz1;
import  java.util.Scanner;

public class Singleton
{
    public String str;
    private static Singleton single_instance = null;
    private Singleton()
    {
        str = "Hello I am a string part of Singleton class";

    }

    public static Singleton getInstance()
    {
        if (single_instance == null)
            single_instance = new Singleton();


        return single_instance;
    }
}

class Main
{

    public static void main(String[] args)
    {
        Singleton x = Singleton.getInstance();
        Singleton y = Singleton.getInstance();
        Singleton z = Singleton.getInstance();

        System.out.println("String from x is " + x.str);
        System.out.println("String from y is " + y.str);
        System.out.println("String from z is " + z.str);
        System.out.println("\n");
    }
}