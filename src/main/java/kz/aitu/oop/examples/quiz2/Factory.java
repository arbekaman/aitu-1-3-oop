package kz.aitu.oop.examples.quiz2;

import java.util.Scanner;

interface Food {
        public String getType();
    }
    class Pizza implements Food {
        public String getType() {
            return "Someone ordered Fast Food!";
        }
    }

    class Cake implements Food {

        public String getType() {
            return "Someone ordered Dessert!";
        }
    }
    class FoodFactory {
        public Food getFood(String order) {
            if (order.equalsIgnoreCase("cake")) {
                Food c = new Cake();
                return c;
            }
            else {
                Food p = new Pizza();
                return p;
            }

        }

    }

    public class Factory{

        public static void main(String[] args) {
            Scanner sc=new Scanner(System.in);

            FoodFactory foodFactory = new FoodFactory();


            Food food = foodFactory.getFood(sc.nextLine());


            System.out.println("The factory returned "+food.getClass());
            System.out.println(food.getType());
        }

    }